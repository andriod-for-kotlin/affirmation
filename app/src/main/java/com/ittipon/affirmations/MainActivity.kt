package com.ittipon.affirmations

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.ittipon.affirmations.adapter.ItemAdapter
import com.ittipon.affirmations.data.Datasource

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val myDataset = Datasource().loadAffirmations()
        val recycleView = findViewById<RecyclerView>(R.id.recycle_view)
        recycleView.adapter = ItemAdapter(this, myDataset)
        recycleView.setHasFixedSize(true)
    }
}