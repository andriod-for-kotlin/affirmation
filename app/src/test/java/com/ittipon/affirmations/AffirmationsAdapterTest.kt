package com.ittipon.affirmations

import android.content.Context
import com.ittipon.affirmations.adapter.ItemAdapter
import com.ittipon.affirmations.model.Affirmations
import org.junit.Assert.assertEquals
import org.junit.Test
import org.mockito.Mockito.mock

class AffirmationsAdapterTest {
    private val context = mock(Context::class.java)
    @Test
    fun adapter_size(){
        val data = listOf(
            Affirmations(R.string.affirmation1, R.drawable.image1),
            Affirmations(R.string.affirmation2, R.drawable.image2)
        )
        val adapter = ItemAdapter(context, data)
        assertEquals("ItemAdapter is not the correct size", data.size, adapter.itemCount)


    }
}